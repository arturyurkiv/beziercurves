﻿using UnityEngine;
using System.Collections;

public class BezierCurvesComponent : MonoBehaviour {

	public Transform AdjustPoint, EndPoint, AdjustMirror;
	[SerializeField] private Color _color = Color.white;
	[SerializeField] private float _scale = 1;

	void OnDrawGizmos()
	{
		Gizmos.color = _color;
		Gizmos.DrawCube(transform.position, Vector3.one * _scale);
	}
}
