﻿
using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class BezierCurvesAdjustment : MonoBehaviour {

	[SerializeField] private Transform _mirror, _parent;
	[SerializeField] private Color _color = Color.white;
	[SerializeField] private float _scale = 1;

	void OnDrawGizmos()
	{
		Gizmos.color = _color;
		Gizmos.DrawCube(transform.position, Vector3.one * _scale);
		Gizmos.DrawSphere(_mirror.position, _scale/2);
		Gizmos.DrawLine(transform.position, _mirror.position);
	}

	#if UNITY_EDITOR
	void LateUpdate()
	{
		_mirror.position = _parent.position + (transform.localPosition * -1);
	}
	#endif
}
