﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class BezierCurves : MonoBehaviour {

	[SerializeField] private BezierCurvesComponent _prefab;
	[SerializeField] private Vector3 _offset; 
	[SerializeField] private int _segmentCount = 25; 
	[SerializeField] private LineRenderer line;

	private BezierCurvesComponent[] _point;
	private Vector3[] _bezierPath; 

	private BezierCurvesComponent last;
	private Vector3 lastPos;
	private int id;

	
	/// <summary>
	/// Add new point
	/// </summary>
	public void AddPoint()
	{
		_point = new BezierCurvesComponent[transform.childCount + 1];

		for(int j = 0; j < transform.childCount; j++)
		{
			_point[j] = transform.GetChild(j).GetComponent<BezierCurvesComponent>();
		}

		if (last)
		{
			lastPos = last.transform.position;
		}
		
		last = Instantiate(_prefab) as BezierCurvesComponent;
		last.gameObject.name = "BezierPoint_" + id;
		
		if(_point.Length > 1) last.transform.position = lastPos + _offset; else last.transform.position = transform.position;
		{
			last.transform.parent = transform;
		}
		_point[_point.Length - 1] = last;

		id++;
	}

	/// <summary>
	/// Destroy all points
	/// </summary>
	public void ClearAll() 
	{
		for(int i = 0; i < _point.Length; i++)
		{
			if (_point[i])
			{
				DestroyImmediate(_point[i].gameObject);
			}
		}

		_point = new BezierCurvesComponent[0];
		id = 0;
		DrawCurves();
	}

	/// <summary>
	/// Destroy last point
	/// </summary>
	public void DestroyLast() 
	{
		if(last == null)
		{
			return; 
		}
		DestroyImmediate(last.gameObject);

		lastPos -= _offset;

		_point = new BezierCurvesComponent[transform.childCount];
		
		for(int j = 0; j < transform.childCount; j++)
		{
			_point[j] = transform.GetChild(j).GetComponent<BezierCurvesComponent>();
			last = _point[j];
		}

		id--;
		DrawCurves();
	}

	#if UNITY_EDITOR 
	private void LateUpdate()
	{
		if (_point.Length < 2 || _segmentCount < 6)
		{
			return;
		} 
		DrawCurves();
	}
	#endif

	/// <summary>
	/// Draw curvet 
	/// </summary>
	public void DrawCurves() 
	{
		bool sw = true;
		List<Vector3> pointRight = new List<Vector3>();
		List<Vector3> pointLeft = new List<Vector3>();
		
		for(int i = 1; i < _point.Length; i++)
		{
			if(pointRight.Count == 0)
			{
				pointRight.Add(_point[i-1].EndPoint.position);
				pointRight.Add(_point[i-1].AdjustPoint.position);
				pointRight.Add(_point[i].AdjustPoint.position);
			}
			else if(!sw)
			{
				pointRight.Add(_point[i-1].AdjustMirror.position);
				pointRight.Add(_point[i].AdjustMirror.position);
			}
			else
			{
				pointRight.Add(_point[i-1].AdjustPoint.position);
				pointRight.Add(_point[i].AdjustPoint.position);
			}

			pointRight.Add(_point[i].EndPoint.position);
			sw = !sw;
		}


		for(int i = 0; i < pointRight.Count - 3; i += 3)
		{
			if(pointLeft.Count > 0) pointLeft.RemoveAt(pointLeft.Count - 1); 
			for(int j = 0; j <= _segmentCount; j++)
			{
				float t = (float)j/_segmentCount;
				Vector3 pxl = CalculateBezierPoint(t, pointRight[i], pointRight[i + 1], pointRight[i + 2], pointRight[i + 3]);
				pointLeft.Add(pxl);
			}
		}



		_bezierPath = new Vector3[]{};
		_bezierPath = pointLeft.ToArray();
		line.SetVertexCount(_bezierPath.Length);
		line.SetPositions(_bezierPath);
	}

	private Vector3 CalculateBezierPoint(float t, Vector3 point0, Vector3 point1, Vector3 point2, Vector3 point3)
	{
		float u = 1 - t;
		float tt = t * t;
		float uu = u * u;
		float uuu = uu * u;
		float ttt = tt * t;

		Vector3 point = uuu * point0;
		point += 3 * uu * t * point1;
		point += 3 * u * tt * point2;
		point += ttt * point3;
		return point;
	}
}
